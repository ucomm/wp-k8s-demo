# WP K8S Demo

## Requirements

- [Docker desktop with kubernetes](https://docs.docker.com/desktop/kubernetes/) active or another local kubernetes installation (e.g. [minikube](https://minikube.sigs.k8s.io/docs/))

### Helpful Tools
- [kubectx](https://github.com/ahmetb/kubectx) - easier context switching for k8s
- [kubeseal](https://github.com/bitnami-labs/sealed-secrets/releases)
- [k9s](https://k9scli.io/) - manage clusters more easily

### MS Azure
- [Install the Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)
- get the credentials for the wp-k8s-demo project
```bash
az aks get-credentials --resource-group wp-k8s-demo --name wp-k8s-demo
```
- test that it works
```bash
kubectl get nodes
# should return an aks node(s)
```
- view the project via the wordpress Service IP address
```bash
kubectl get service wordpress
# should return a service with an external IP address and port
```

## Structure

### base
The `/base` directory contains the default configuration for the installation.

### overlays
Currently the only overlay is in `/overlays/production`. Files registered in that directory's kustomization.yaml override configuration in a corresponding base file.

## Usage

```bash
cd {this directory}

# use the kubernetes cli to provision the wp installation based on the files in this project
kubectl apply -k base

# if changing a single file
kubectl apply -f filename.yaml
```

The installation will be available at [localhost:30000](http://localhost:30000).

The `kustomization.yaml` file is a manifest to help organize the various resources of the k8s project.

### Secrets
Using k8s secrets aren't particularly secure. They're just base64 encoded values. Using something like kubeseal is needed. To create a sealed secret yaml file

```
kubectl create secret generic mysql-credentials --from-env-file=.env.base -o yaml --dry-run=client | kubeseal --scope cluster-wide -o yaml > ./mysql-credentials.yaml
```

This will
- create a generic secret called `mysql-credentials` from a file. 
- output the content as yaml, but don't save it (--dry-run)
- pipe to `kubeseal` to create a scoped secret

### Media Import
Copy media files into a temp directory in the `/usr/src/wordpress` directory. Then import using glob patterns

```bash
# on local device (add a . at the end of the local path to copy just the contents)
kubectl cp ~/local-dir-path/. pod-ID:/usr/src/wordpress/wp-content/uploads

# change the urls of the images in the db to match the new uploads directory
wp search-replace http://original.url/content http://new.url/wp-content wp_posts
```

## Terms

### Service
A routing layer for the installation. These files describe how pods will be networked with each other and with the outside world (as needed). 

### Persistent Volume Claim (PVC)
This represent persistent storage (e.g. a file system). This ensures that even if the database service and/or pods are taken down, the data itself will persist.

### Deployment
These files represent configuration of the actual pods and the containers within them. Note that they route data storage and networking _within_ the k8s node. For instance, the `WORDPRESS_DB_HOST` in the `wordpress-deployment.yaml` file is keyed to `wordpress-mysql` which is the name listed in the metadata of the `mysql-deployment.yaml` file 

## TODO
- multiple sites